package cn.zhuangcloud.common.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

public class AuthenticationInterceptor implements Interceptor {

    public void intercept(Invocation inv) {
        Controller controller=inv.getController();
        controller.setAttr("username","zhengzhaoyu");
        inv.invoke();
    }

}
