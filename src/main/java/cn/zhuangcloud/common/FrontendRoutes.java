package cn.zhuangcloud.common;

import cn.zhuangcloud.page.index.IndexController;
import com.jfinal.config.Routes;

public class FrontendRoutes extends Routes{

    public void config() {
        setBaseViewPath("/view");
        add("/index", IndexController.class,"/index");
    }

}
