package cn.zhuangcloud.common;

import cn.zhuangcloud.api.login.LoginAPI;
import com.jfinal.config.Routes;
import com.jfinal.ext.interceptor.POST;

public class BackstageRoutes extends Routes{

    public void config() {
        addInterceptor(new POST());
        add("/api/login", LoginAPI.class);
    }

}
