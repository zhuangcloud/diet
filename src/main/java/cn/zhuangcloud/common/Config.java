package cn.zhuangcloud.common;

import cn.zhuangcloud.common.interceptor.AuthenticationInterceptor;
import cn.zhuangcloud.common.model._MappingKit;
import com.alibaba.druid.filter.logging.Slf4jLogFilter;
import com.jfinal.config.*;
import com.jfinal.core.JFinal;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.template.Engine;

public class Config extends JFinalConfig {

    public static void main(String[] args) {
        JFinal.start("src/main/webapp", 80, "/");
    }

    public void configConstant(Constants me) {
        // 加载少量必要配置，随后可用PropKit.get(...)获取值
        PropKit.use("ProConfig.properties").appendIfExists("DevConfig.properties");
        me.setDevMode(PropKit.getBoolean("devMode", false));
    }

    public void configRoute(Routes me) {
        me.add(new FrontendRoutes());
        me.add(new BackstageRoutes());
    }

    public void configEngine(Engine me) {

    }

    public static DruidPlugin createDruidPlugin() {
        DruidPlugin druidPlugin = new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
        druidPlugin.addFilter(new Slf4jLogFilter());
        return druidPlugin;
    }

    public void configPlugin(Plugins me) {
        // 配置 druid 数据库连接池插件
        DruidPlugin druidPlugin = createDruidPlugin();
        me.add(druidPlugin);

        // 配置ActiveRecord插件
        ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
        // 所有映射在 MappingKit 中自动化搞定
        _MappingKit.mapping(arp);
        me.add(arp);
    }

    public void configInterceptor(Interceptors me) {
        me.add(new AuthenticationInterceptor());
    }

    public void configHandler(Handlers me) {

    }

}
